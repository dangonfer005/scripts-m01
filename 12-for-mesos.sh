#! /bin/bash
#
#Daniel González Ferrer
#
#Descripción: Programa que rep almenys un més  o varis mesos. 
#Per cada més indica quants dies té el més. 
#	a) el programa valida que el número d'arguments rebuts és vàlid.
#	b) el programa valida per cada més que el seu valor és del 1 al 12. 
# 	Si no ho és es mostra un missatge d'error per stderr però es continua processant la resta de mesos.
#	c) per cada més s'indica quants dies té
#-----------------------------------------------------------------------------------

ERR_ARGS=1
#1) Validar argumentos
if [ $# -lt 1 ]
then
	echo "Error: Numero de argumentos invalido"
	echo "Usage: $0 meses [1-12]"
	exit $ERR_ARGS
fi
# 2) Validar mes
num=1
for mes in "$@"
do
	if [ $mes -lt 1 -o $mes -gt 12 ]
	then
		echo "Error: mes$num: $mes invalido"
		echo "Usage: $0 meses [1-12]"
	else
		case $mes in
			"2")
			   dias=28;;
			"4"|"6"|"9"|"11")
			   dias=30;;
			*)
			   dias=31;;
	    esac
	   echo "Mes$num: $mes, tiene $dias dias" 
	fi
	((num++))
done
exit 0