#! /bin/bash
#Daniel González Ferrer
#
#Llista el direcroti rebut
#	a) verifica rep arg
#	b) verifica que es un directori
#------------------------------------------
ERR_ARGS=1
ERR_NODIR=2
# 1) verificar rep arg
if [ $# -ne 1 ]
then
  echo "Error: numero de argumentos invalido"
  echo "Usage: $0 dir"
  exit $ERR_ARGS
fi
# 2) Verificacion de directorio
if ! [ -d $1 ]
then
  echo "Error: $1 No es un directorio"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi
# 3) chicha
dir=$1
ls $dir
exit 0

