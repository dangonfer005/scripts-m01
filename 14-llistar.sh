#a
#! /bin/bash
# Daniel González Ferrer
# 26/04/2024
# prog dir
# rep un arg i es un directori i es llista
# -m----------------------------------------
# 1) validar arg
ERR_ARG=1
if [ $# -eq 0 ]; then
  echo "Error numero arguments"
  echo "Usage: $0 dir"
  exit $ERR_ARG
fi
dir=$1
# 2) validar dir
ERR_DIR=2
if [ ! -d $dir ];then
  echo "Error $dir no es directori"
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi
# 3) llistar
ls $dir
exit 0
#b
#! /bin/bash
# Daniel González Ferrer
# 26/04/2024
# prog dir
# a- rep un arg i es un directori i es llista
# b- llistar numerant els elements del dir
# ------------------------------------------
# 1) validar arg
ERR_ARG=1
if [ $# -eq 0 ]; then
  echo "Error numero arguments"
  echo "Usage: $0 dir"
  exit $ERR_ARG
fi
dir=$1
# 2) validar dir
ERR_DIR=2
if [ ! -d $dir ];then
  echo "Error $dir no es directori"
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi
# 3) llistar
llistat=$(ls $dir)
i=1
for elem in $llistat
do
  echo "$i: $elem"
  ((i++))
done
exit 0
#c
#! /bin/bash
# Daniel González Ferrer
# 26/04/2024
# prog dir
# a- rep un arg i es un directori i es llista
# b- llistar numerant els elements del dir
# c- dir i es dir, regular o altres
# ------------------------------------------
# 1) validar arg
ERR_ARG=1
if [ $# -eq 0 ]; then
  echo "Error numero arguments"
  echo "Usage: $0 dir"
  exit $ERR_ARG
fi
dir=$1
# 2) validar dir
ERR_DIR=2
if [ ! -d $dir ];then
  echo "Error $dir no es directori"
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi
# 3) llistar
llistat=$(ls $dir)
for elem in $llistat
do
  if [ -f $dir/$elem ];then
    echo "$elem is regular file"
  elif [ -d $dir/$elem ];then
    echo "$elem is a directory"
  else
    echo "$elem is another thing"
fi
done
exit 0
#d
#! /bin/bash
# Daniel González Ferrer
# 26/04/2024
# prog dir
# a- rep un arg i es un directori i es llista
# b- llistar numerant els elements del dir
# c- dir i es dir, regular o altres
# ------------------------------------------
# 1) validar arg
ERR_ARG=1
if [ $# -eq 0 ]; then
  echo "Error numero arguments"
  echo "Usage: $0 dir"
  exit $ERR_ARG
fi
dir=$1
# 2) validar dir
ERR_DIR=2
if [ ! -d $dir ];then
  echo "Error $dir no es directori"
  echo "Usage: $0 dir"
  exit $ERR_DIR
fi
# 3) llistar
llistat=$(ls $dir)
for elem in $llistat
do
  if [ -f $dir/$elem ];then
    echo "$elem is regular file"
  elif [ -d $dir/$elem ];then
    echo "$elem is a directory"
  else
    echo "$elem is another thing"
fi
done
exit 0


