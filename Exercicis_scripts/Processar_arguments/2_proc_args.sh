#! /bin/bash
#
#Daniel González Ferrer
#Enero 2024
#
#Descripcio: Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
#-------------------------------------------------------------------------------------------

char3=0
for arg in $*
do
  echo "$arg" | grep -q -E ".{3,}"
  if [ $? -eq 0 ];then
    ((char3++))
  fi
done
echo "$char3"
exit 0
