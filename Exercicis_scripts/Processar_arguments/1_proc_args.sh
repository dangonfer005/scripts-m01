#! /bin/bash
#
#Daniel González Ferrer
#Enero 2024
#
#Descripcio: Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#-------------------------------------------------------------------------------------------

for arg in $*
do
  echo "$arg" | grep -E ".{4,}"
done
exit 0
