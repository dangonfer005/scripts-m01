#! /bin/bash
#
#Daniel González Ferrer
#Enero 2024
#
#Descripcio: Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
#-------------------------------------------------------------------------------------------

for matricula in $*
do
  echo $matricula | grep -q -E [0-9]{4}-[A-Z]{3}
