#! /bin/bash
# Daniel González Ferrer
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 6.- dia laborable-festiu
# ---------------------------
# validar arg
ERR_ARG=1
if [ $# -eq 0 ]; then
  echo "Error argument no valid"
  echo "Usage: $0 dia..."
  exit $ERR_ARG
fi
# dia laborable-festiu o no
laborable=0
festiu=0
for dia in "$@"; do
	case $dia in
    	dilluns|dimarts|dimecres|dijous|divendres)
		((laborable++));;
    	dissabte|diumenge)
		((festiu++));;
    	*)
        	echo "$dia no es un dia de la setmana">>/dev/stderr
	esac
done	
echo "Laborables: $laborable"
echo "Festius: $festiu"
exit 0	

