#! /bin/bash
#
#Daniel González Ferrer
#Febrer 2024
#
#Descripcio: 1. Mostrar l’entrada estàndard numerant línia a línia
#----------------------------------------------------------------
#1) xixa
cont=1
while read -r line
do
  echo "$cont: $line"
  ((cont++))
done
exit 0
