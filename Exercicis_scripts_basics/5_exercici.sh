#! /bin/bash
#
#Daniel González Ferrer
#Enero 2024
#
#Descripcio: 5. Mostrar línia a línia l’entrada estàndard, retallant només els primers 50 caràcters.
#---------------------------------------------------------------------------------------------------
#1) xixa
while read -r line
do
  echo "$line" | cut -c-50
done
exit 0

