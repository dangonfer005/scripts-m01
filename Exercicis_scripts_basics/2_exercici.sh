#! /bin/bash
#
#Febrer 2024
#
#Descripcio: 2. Mostar els arguments rebuts línia a línia, tot numerànt-los.
#
#------------------------------------------------------------------------------------
#1) Validacion de argumentos
ERR_NARGS=1
if [ $# -lt 1 ]
then
  echo "Error: Numero de argumentos invalido"
  echo "Usage: $0 agr"
  exit $ERR_NARGS
fi
#2) xixa
cont=1
for agr in $*
do
  echo "$cont: $agr"
  ((cont++))
done
exit 0
