#! /bin/bash
# Daniel González Ferrer
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 10.- processar stdin linia a linia i mostra numerades un max de linies amb arg
# --------------------------- 
# validar args
ERR_ARGS=1
if [ $# -ne 1 ];then
	echo "Error numero arguments"
	echo "Usage: $0 num"
	exit $ERR_ARGS
fi
# numerar linia a linia
max=$1
comptador=1
read -r line
while [ $max -ge $comptador ]
do
	echo "$comptador: $line"
	read -r line
	((comptador++))
done
exit 0

