#! /bin/bash
#
#Daniel González Ferrer
#Febrer 2024
#
#Descripcio: 4. Fer un programa que rep com a arguments números de més (un o més) i indica per
#a cada mes rebut quants dies té el més.
#-------------------------------
#1) Validacion  de Argumentos
ERR_NAGRS=1
if [ $# -lt 1 ]
then
  echo "Error: Numero de Argumentos invalido"
  echo "Usage: $0 meses [1-12]"
  exit 0
fi
#2) Validacion de meses
num=1
for mes in "$@"
do
  if [ $mes -lt 1 -o $mes -gt 12 ]
  then
    echo "Error: mes$num: $mes invalido" >> /dev/stderr
  else
    case $mes in
	    "2") 
	     dias=28;;
     "4"|"6"|"9"|"11")
	     dias=30;;
             *)
             dias=31;;
    esac
  echo "Mes$num: $mes, tiene $dias dias"
  fi
  ((num++))
done
exit 0
