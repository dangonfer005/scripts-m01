#! /bin/bash
# Daniel González Ferrer
# 19/02/2024
#
# Exercicis d'scripts bàsics.
# 7.- processar lini a linia entrada estandar de > 60 caracters
# ---------------------------
# compte de caracters i mostra o no
MIN=60
while read -r line
do
	num_caracters=$(echo "$line" | wc -c)
	if [ $num_caracters -ge $MIN ]; then
		echo "$line"
	fi
done
exit 0

