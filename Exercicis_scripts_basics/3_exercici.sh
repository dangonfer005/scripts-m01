#! /bin/bash
#
#Daniel González Ferrer
#Febrer 2024
#
#Descripcio: 3. Fer un comptador des de zero fins al valor indicat per l’argument rebut.
#---------------------------------------------------------------------------------------
#1) Validacion de argumentos
ERR_NARGS=1
if [ $# -ne 1 ]
then
  echo "Error: numero argumento invalido"
  echo "Usage $0 num"
  exit $ERR_NARGS
fi
#2) xixa
cont=0
max=$1
while [ $cont -le $max ]
do
  echo "$cont"
  ((cont++))
done
exit 0
