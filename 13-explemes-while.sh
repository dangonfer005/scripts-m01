#! /bin/bash
#
#Daniel Gonzalez Ferrer
#
#exemple while
#
#-------------------------------

#8) procesar linea a linea un file
fileIN=$1
num=1
while read -r line
do
	chars=$(echo $line | wc -c)
	echo "$num: ($chars) $line" | tr 'a-z' 'A-Z'
  ((num++))
done < $fileIN
exit 0

#7) numerar i mostrar en majuscule
# stdin
num=1
while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num++))
done
exit 0

#6) iterar linia linia fins token
TOKEN="FI"
read -r line
while [ "$line" != $TOKEN ]
do
  echo "$line"
  read -r line
done
exit 0

#5) numerar les lineas rebudes
i=1
while read -r line
do
  echo "$i: $line"
  ((i++))
done
exit 0

#4) while 'read -r $' .....do....done
#Procesa linea a linia la entrada estandar

while read -r line
do
  echo "$line"
done
exit 0

#3) iterar listas de argumentos
while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0

#2)contador decrementar valor rebut
#	narg....0
MIN=0
num=$1
while [ $num -ge $MIN ]
do 
  echo -n "$num "
  ((num--))
done
exit 0

#1) mostra numeros del 1 al 10
MAX=10
num=1
while [ $num -le $MAX ]
do
  echo -n "$num"
  ((num++))
done
exit 0
