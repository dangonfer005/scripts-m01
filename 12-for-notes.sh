#! /bin/bash
#
#Daniel González Ferrer
#
#Descripción: Programa que rep almenys una nota o més  i per cada nota diu 
#si és un suspès, aprovat, notable o excel·lent.
#		a) el programa valida que el número d'arguments és correcte.
#		b) per cada nota valida que el valor és entre 0 i 10. Si no és 
#		correcte es mostra un missatge per stderr però es continua iterant la resta de notes.
#		c) per cada nota vàlida mostra quina qüalificació li correspòn.
#-----------------------------------------------------------------------------------
 ERR_ARGS=1
#1) Validar argumentos
if [ $# -lt 1 ]
then
  echo "Error: Minimo 1 argumento"
  echo "Usage: $0 note/s [0-10]"
  exit $ERR_ARGS
fi

#2)validar nota
num=1
for nota in $*
do 
  if  [ $nota -lt 0 -o $nota -gt 10 ]
  then
    echo "Error nota $num: $nota, invalida [0-10]" >> /dev/stderr
  else

    if [ $nota -eq 5 -o $nota -eq 6 ]
    then
      echo "La nota $num: $nota, es un aprobado" >> notas.log
    elif [ $nota -eq 7 -o $nota -eq 8 ]
    then
      echo "La nota $num: $nota, es un notable" >> notas.log
    elif [ $nota -eq 9 -o $nota -eq 10 ]
    then
      echo "La nota $num: $nota, es un Exelente" >> notas.log
    else
      echo "La nota $num: $nota, es un suspenso" >> notas.log
    fi
  fi
  ((num++))
done
exit 0
