#! /bin/bash
#Daniel González Ferrer
#
#Llista el direcroti rebut
#	a) verifica rep arg
#	b) verifica que es un: regular, dir, link
#	otra cosa
#------------------------------------------
ERR_ARGS=1
ERR_NOEXITS=2
# 1) verificar rep arg
if [ $# -ne 1 ]
then
  echo "Error: numero de argumentos invalido"
  echo "Usage: $0 file"
  exit $ERR_ARGS
fi
# 2) Verificacion de file
file=$1
if [ ! -e $file ]
then
  echo "$file no existe"
  exit "$ERR_NOEXITS"
elif [ -d $file ]
then
  echo "El file: $file es un directorio"
elif [ -h $file ]
then
  echo "El file: $file es un link"
elif [ -f $file ]
then
  echo "El file: $file es un fichero regular"
else
  echo "$file es otra cosa"
fi
exit 0
