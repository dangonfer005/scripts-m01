#! /bin/bash
#Daniel González Ferrer
#Enero 2024
#
#Exemple orden if
#---------------------------
#
#1) Validar argumentos
if [ $# -ne 1 ]
then
  echo "Error: Numero de argumento incorrecto"
  echo "Usage: $0 edat"
  exit 1
fi
#2)chicha
edat=$1 
if [ $edat -lt 18 ]
then
  echo "edat $edat es menor de edad"
elif [ $edat -lt 65 ]
then
  echo "edat $edat es edad activa"
else
  echo "edar $edat es jubilado"
fi
exit 0
