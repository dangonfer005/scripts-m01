#! /bin/bash 
#
#Daniel González Ferrer
#Enero 2024 
#
#Exemple case
#-------------------------------
#
#exemple vocals

#ejemplo 2)

case $1 in
  "dl" | "dt" | "dc" | "dj" | "dv")
    echo "$1 es laborable"
    ;;
  "ds" | "dm")
    echo "$1 es festivo"
    ;;
  *)
    echo "$1 no es un día"
esac
exit 0


#ejemplo 1)

case $1 in
  [aeiou])
    echo "$1 es una vocal"
    ;;
  [bcdfghjklmnpqrsvwxyz])
    echo "$1 es una consonante"
    ;;
  *)
    echo "$1 es una otra cosa"
    ;;
esac
exit 0
