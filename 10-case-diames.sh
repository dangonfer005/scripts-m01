#! /bin/bash
#
#Daniel González Ferrer
#Enero 2024
#
# di el dia que tiene un mes
# synopsis: prog mes
# 	a) validar rep arg
# 	b) validar mes [1-12]
# 	c) xixa
#-------------------------------

#1) validar rep arg
ERR_NARGS=1
ERR_MES=2
if [ $# -ne 1 ]
then
  echo "Error: argumentos invalidos"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi
#2) validar mes [1-12]
if [ $1 -ge 13 -o $1 -le 0 ]
then
  echo "Error: mes $1 invalido"
  echo "Mes tiene que ser [1-12]"
  echo "Usage: $0 mes"
  exit $ERR_MES
fi
#3) xixa
mes=$1
case $mes in
  "2")
    dias=28;;
  "4"|"6"|"9"|"11")
    dias=30;;
   *)
    dias=31;;
esac
echo "El mes $mes, tiene $dias dias"
exit 0
