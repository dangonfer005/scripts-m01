#! /bin/bash
# Daniel González Ferrer
# Febrer 2024
#
# validar argumentos
# ---------------------------
#
# 1) valida que hay 2 argumentos 
if [ $# -ne 2 ]
then
  echo "Error: argumentos invalidos"
  echo "Usage: $0 nom y cognom"
  exit 1
fi

# 2) chicha que se muestra
echo "nom: $1"
echo "cognom: $2"
exit 0
