#! /bin/bash
# Daniel González Ferrer
# Febrer 2024
#Validar nota: súspes, aprovat
#	a) rep un argument
#	b) es del 0 al 10
# ---------------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) validar numero de argumentos
if [ $# -ne 1 ]
then
  echo "Error: numero de argumentos invalido"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi
# 2) validar argumento [0,10]
if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Error: nota $1 no valida"
  echo "nota para el valor del 0 al 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi
#3) chicha
nota=$1
if [ $nota -eq 5 -o $nota -eq 6 ]
then 
  echo "Nota $nota: Aprovat"
elif [ $nota -eq 7 -o $nota -eq 8 ]
then
  echo "Nota $nota: Notable"
elif [ $nota -eq 9 -o $nota -eq 10 ]
then
  echo "Nota $nota: Exelente"
else
  echo "Nota $nota: Súspes"
fi
exit 0
