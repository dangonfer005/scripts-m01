#! /bin/bash
#
#Daniel González Ferrer
#Enero 2024
#
#Descripcio: ejemplos bucle for
#-------------------------------


#8)lista todos los login ordenados por login
lista=$(cut -d: -f1 /etc/passwd | sort)
num=1
for elem in $lista
do
  echo "$num: $lista"
  ((num++))
done
exit 0

#7)lista y enumera las lineas
lista=$(ls)
num=1
for elem in $lista
do
  echo "$num: $elem"
  ((num++))
done
exit 0


#6) iterar por cada uno de los valores de ls
lista=$(ls)
for elem in "$lista"
do
  echo "$elem"
done
exit 0


#5) numerar argumentos
num=1
for arg in "$@"
do
  echo "$num: $arg"
  ((num++))
done
exit 0


#4) $@ expandir $* no

for arg in "$@"
do
  echo "$arg"
done
exit 0


#3)iterar por la lista de argumentos
for arg in "$*"
do
  echo "$arg"
done
exit 0



#2) Iterar nombres
for nom in "pere marta anna pau"
do
  echo "$nom"
done
exit 0


#1) iterar nombres
for nom in "pere" "marta" "anna" "pau"
do
  echo "$nom"
done
exit 0
